﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;

namespace UnequalPartition.BucketCreation
{
    /*
     * The non sensitve size is 150,000
     * so y= 375 and x = 400
     *
     */
    public class CreateBucket
    {

        static string connectionString = @"UnequalPartition.Properties.Settings.SQLServCon";
        public static void SetupConnection()
        {
            string sConnection = Properties.Settings.Default.SQLServCon;
            connectionString = sConnection;


            Console.WriteLine(connectionString);

            // Create a SqlConnectionStringBuilder.
            SqlConnectionStringBuilder connStringBuilder = new SqlConnectionStringBuilder(connectionString);

            // Enable Always Encrypted for the connection.
            // This is the only change specific to Always Encrypted
            connStringBuilder.ColumnEncryptionSetting = SqlConnectionColumnEncryptionSetting.Enabled;

            Console.WriteLine(Environment.NewLine + "Updated connection string with Always Encrypted enabled:");
            Console.WriteLine(connStringBuilder.ConnectionString);


            // Assign the updated connection string to our global variable.
            connectionString = connStringBuilder.ConnectionString;
        }

        public void CreateBuckets(string schema, string fileNameSuffix, int nSSize, int sSize)
        {
            this.Schema = schema;
            this.FileNameSuffix = fileNameSuffix;
            this.NSSize = nSSize;
            this.SSize = sSize;
            HandleSensitiveKeys();
            HandleNonSensitiveKeys();
        }



        private Dictionary<decimal, int> SKeyBucketDict = new Dictionary<decimal, int>();
        private List<List<decimal>> SKeyBuckets = new List<List<decimal>>();

        private Dictionary<decimal, int> NSKeyBucketDict = new Dictionary<decimal, int>();
        private List<List<decimal>> NSKeyBuckets = new List<List<decimal>>();

        private string Schema = "TPC10PERCENT";
        private string FileNameSuffix = "10";
        private int NSSize = 400;
        private int SSize = 375;



        private void HandleNonSensitiveKeys()
        {
            List<decimal> NSKeys = GetAllCustKeyFromNonSensitive();
            NSKeys.Sort();

            FillUpDictAndBucketsForNonSensitive(NSKeys, NSSize );
            SerializeData(NSKeyBucketDict, "NSKeyBucketDict_"+ FileNameSuffix+".osl");
            SerializeData(NSKeyBuckets, "NSKeyBuckets_"+ FileNameSuffix+".osl");
        }

        private void FillUpDictAndBucketsForNonSensitive(List<decimal> NSKeys, int BucketSize)
        {
            int NumberOfBuckets = (int)Math.Ceiling((double)NSKeys.Count / (double)BucketSize);
            for (int i = 0; i < NumberOfBuckets; i++)
            {
                NSKeyBuckets.Add(new List<decimal>());
            }


            HashSet<decimal> NSkeysHashSet = new HashSet<decimal>(NSKeys);
            //first fill up the NS keys for each of the corresponding S keys
            //we know that each S key is also present in NS as well while the converse is not true
            //but we have added the check that each S key is present in NS before adding the key
            foreach(List<decimal> ABucket in SKeyBuckets)
            {
                for(int i=0; i<ABucket.Count; i++)
                {
                    decimal k = ABucket[i];
                    if(NSkeysHashSet.Contains(k) == true)
                    {
                        NSKeyBucketDict.Add(k, i);
                        NSKeyBuckets[i].Add(k);
                    }

                }
            }
            NSkeysHashSet = null;
            //now fill up all the other keys
            int CurrBucket = 0;
            foreach (decimal AKey in NSKeys)
            {
                //ignore the  ones that have been already filled up
                if (NSKeyBucketDict.ContainsKey(AKey) == false)
                {
                    if(NSKeyBuckets[CurrBucket].Count == BucketSize)
                    {
                        CurrBucket += 1;
                    }
                    NSKeyBucketDict.Add(AKey, CurrBucket);
                    NSKeyBuckets[CurrBucket].Add(AKey);
                }

                
            }
            

        }

        private List<decimal> GetAllCustKeyFromNonSensitive()
        {
            List<decimal> AllCustKeys = new List<decimal>();

            SqlCommand sqlCmd = new SqlCommand(
                "SELECT [C_CUSTKEY] FROM  [TPCPERCENT].[CUSTOMER_NS] ",
                new SqlConnection(connectionString));
            using (sqlCmd.Connection = new SqlConnection(connectionString))
            {

                try
                {
                    sqlCmd.Connection.Open();
                    SqlDataReader reader = sqlCmd.ExecuteReader();

                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            AllCustKeys.Add(reader.GetDecimal(0));

                        }
                    }


                }
                catch (Exception ex)
                {
                    throw ex;
                }

            }

            return AllCustKeys;


        }

        private void HandleSensitiveKeys()
        {
            List<decimal> SKeys = GetAllCustKeyFromSensitive();
            SKeys.Sort();
            FillUpDictAndBucketsForSensitive(SKeys, SSize);
            SerializeData(SKeyBucketDict, "SKeyBucketDict_" + FileNameSuffix + ".osl");
            SerializeData(SKeyBuckets, "SKeyBuckets_" + FileNameSuffix + ".osl");

        }

        private void FillUpDictAndBucketsForSensitive(List<decimal> Keys, int size)
        {
            List<decimal> SingleBucket = new List<decimal>();
            int cnt = 0;
            int BucketCount = 0;
            foreach (decimal Akey in Keys)
            {
                if (cnt == size)
                {
                    BucketCount += 1;
                    cnt = 0;
                    SKeyBuckets.Add(SingleBucket);
                    SingleBucket = new List<decimal>();
                }
                SingleBucket.Add(Akey);
                SKeyBucketDict.Add(Akey, BucketCount);
                cnt += 1;

            }
            if (SingleBucket.Count > 0)
            {
                SKeyBuckets.Add(SingleBucket);
            }
        }

        private void SerializeData(Object obj, string FileName)
        {
            Stream stream = File.Open(FileName, FileMode.Create);
            BinaryFormatter bformatter = new BinaryFormatter();

            Console.WriteLine("Writing to file " + FileName);
            bformatter.Serialize(stream, obj);
            stream.Close();
        }

        private List<decimal> GetAllCustKeyFromSensitive()
        {
            List<decimal> AllCustKeys = new List<decimal>();

            SqlCommand sqlCmd = new SqlCommand(
                "SELECT [C_CUSTKEY] FROM  ["+Schema+"].[CUSTOMER_S] ",
                new SqlConnection(connectionString));
            using (sqlCmd.Connection = new SqlConnection(connectionString))
            {

                try
                {
                    sqlCmd.Connection.Open();
                    SqlDataReader reader = sqlCmd.ExecuteReader();

                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            AllCustKeys.Add(reader.GetDecimal(0));

                        }
                    }


                }
                catch (Exception ex)
                {
                    throw ex;
                }

            }

            return AllCustKeys;


        }

        
    }
}
