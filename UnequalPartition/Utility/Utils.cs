﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UnequalPartition.Utility
{


    class Utils
    {

        static void Main(string[] args)
        {

            //bool[] primes = MakeSieve(150000);
            
            
            Console.WriteLine(SmallestDiffFactors(150000));

        }

        public static Factors SmallestDiffFactors(int n)
        {
            int p = 1, q = 1;
            int upper = (int)Math.Sqrt(n);
            for(int i=upper; i>1; i--)
            {
                p = i;
                q = n / p;
                if (n == p * q)
                    break;
            }
            if(p < q)
            {
                return new Factors() { y = p, x = q };
            }
            else
            {
                return new Factors() { y = q, x = p };
            }
        }
       

        public static bool[] MakeSieve(int max)
        {
            // Make an array indicating whether numbers are prime.
            bool[] is_prime = new bool[max + 1];
            for (int i = 2; i <= max; i++) is_prime[i] = true;

            // Cross out multiples.
            for (int i = 2; i <= max; i++)
            {
                // See if i is prime.
                if (is_prime[i])
                {
                    // Knock out multiples of i.
                    for (int j = i * 2; j <= max; j += i)
                        is_prime[j] = false;
                }
            }
            return is_prime;
        }
    }
}
