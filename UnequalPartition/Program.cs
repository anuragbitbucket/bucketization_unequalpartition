﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UnequalPartition
{
    class Program
    {
        static void Main(string[] args)
        {
            CreateBucket();
        }

        private static void CreateBucket()
        {
            BucketCreation.CreateBucket.SetupConnection();
            BucketCreation.CreateBucket cb = null;

            cb = new BucketCreation.CreateBucket();
            cb.CreateBuckets("TPC10PERCENT", "10", 400, 375);
            cb = new BucketCreation.CreateBucket();
            cb.CreateBuckets("TPC20PERCENT", "20", 400, 375);
            cb = new BucketCreation.CreateBucket();
            cb.CreateBuckets("TPC30PERCENT", "30", 400, 375);
            cb = new BucketCreation.CreateBucket();
            cb.CreateBuckets("TPC40PERCENT", "40", 400, 375);
            cb = new BucketCreation.CreateBucket();
            cb.CreateBuckets("TPC50PERCENT", "50", 400, 375);
        }
    }
}
